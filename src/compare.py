import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt
from scipy.stats import rayleigh, chi2
from scipy.special import i0
import pandas as pd
import utils
from collections import Counter

def shooting_pdf(x, mu, sigma):
    # mu = 0: rayleigh.pdf(x/(4*sigma)) / (4*sigma)
    # assumes spherically symmetric
    lamb = mu/sigma
    z = x/(4*sigma)
    return i0(lamb*z) * z * np.exp(-(z**2 + lamb**2)/2) /(4*sigma)

shot_list = {"Tor":  utils.generate_list("Tor"),
             "Lars": utils.generate_list("Lars"),
             "Jørn": utils.generate_list("Jørn")}

plt.subplots(2,1)

for name in shot_list.keys():
    dfs = shot_list[name]
    scores = [109 - df["score"].head(40).mean()*10 for df in dfs]

    points = []
    for df in dfs:
        points += df["score"].tolist()
    points = [int(109 - i*10) for i in points]

    cpoints = dict(Counter(points))
    cpoints = dict(sorted(cpoints.items()))

    plt.subplot(2,1,1)
    plt.hist(scores, bins=np.linspace(0,30,4*30+1), label=name, density=True, alpha=0.5)

    plt.subplot(2,1,2)
    plt.hist(points, bins=np.linspace(0,30,30+1), label=name, density=True, alpha=0.5)
    plt.plot(cpoints.keys(), [i/sum(cpoints.values()) for i in cpoints.values()])


plt.subplot(2,1,2)
xs = np.linspace(0.5, 29.5, 30)
plt.plot(xs, shooting_pdf(xs, 0, 1.8))
plt.plot(xs, shooting_pdf(xs, 0, 2.45))

plt.legend()
plt.savefig("figs/probability_distributions.png")
