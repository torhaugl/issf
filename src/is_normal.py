import numpy as np
import matplotlib.pyplot as plt

plt.subplots(1, 3)
plt.subplot(1,3,1)
plt.ylabel("Score")
i = 0
for name in ["Tor", "Lars", "Jørn"]:
    i += 1
    plt.subplot(1,3,i)
    data = np.loadtxt("result/{}_trening.txt".format(name))
    n = data[:,0]
    s = data[:,1]
    plt.hist(s/n, bins=np.linspace(85,105,2*20+1), density=True)
    plt.hist(s[-50:]/n[-50:], bins=np.linspace(85,105,2*20+1), density=True)
plt.savefig("figs/histogram.png", dpi=300)