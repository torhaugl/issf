import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import utils

shot_list = {"Tor":  utils.generate_list("Tor"),
             "Lars": utils.generate_list("Lars"),
             "Jørn": utils.generate_list("Jørn")}

plt.subplots(2,1)

for name in shot_list.keys():
    dfs = shot_list[name]
    scores = [109 - df["score"].head(40).mean()*10 for df in dfs]

    points = []
    for df in dfs:
        points += df["score"].tolist()
    points = [109 - i*10 for i in points]

    plt.subplot(2,1,1)
    plt.hist(scores, bins=np.linspace(0,30,4*30+1), label=name, density=True, alpha=0.5)

    plt.subplot(2,1,2)
    plt.hist(points, bins=np.linspace(0,30,30+1), label=name, density=True, alpha=0.5)

plt.legend()
plt.savefig("figs/probability_distributions.png")
