#!/bin/python
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
from datetime import timedelta, datetime
import utils
import pandas as pd

from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, Slider, Select
from bokeh.models.tools import HoverTool
from bokeh.models.widgets import Panel, Tabs
from bokeh.models import SingleIntervalTicker, NumeralTickFormatter


def set_style(fig):
    fig.yaxis.ticker = SingleIntervalTicker(interval=0.1, num_minor_ticks=1)
    fig.yaxis.formatter = NumeralTickFormatter(format="0.0")

    fig.ygrid.band_hatch_pattern = "/"
    fig.ygrid.band_hatch_alpha = 0.6
    fig.ygrid.band_hatch_color = "lightgrey"
    fig.ygrid.band_hatch_weight = 0.5
    fig.ygrid.band_hatch_scale = 10


FIGURE_CONFIG = {"y_range": [9.0, 10.6],
                 "plot_width": 1000,
                 "plot_height": 600,
                 "y_axis_label": 'Score',
                 "y_axis_location": 'right',
                 "tools": "pan,wheel_zoom,reset"}

HISTOG_CONFIG = {"plot_width": 1000,
                 "plot_height": 600,
                 "y_axis_label": 'Count',
                 "tools": "pan,wheel_zoom,reset"}

def estimate_sd(df_list):
    N = int(N_slider.value)
    mean_score = [df["score"].head(N).mean()
                  for df in df_list]
    sd_list = []
    for i in range(2,len(mean_score)):
        sd = np.std(mean_score[:i])
        sd_list.append(sd)
        print(i, sd)

def generate_data(attrname, old, new):
    df_list = shot_list[name.value]

    estimate_sd(df_list)

    N = int(N_slider.value)
    index = range(-len(df_list), 0)
    date = [df["datetime"][0] for df in df_list]

    if (decimal.value == "Decimal"):
        mean_score = [df["score"].head(N).mean()
                      for df in df_list]
        fig_index.yaxis.bounds = (0.0, 10.9)
        fig_date.yaxis.bounds = (0.0, 10.9)
    elif (decimal.value == "Integer"):
        mean_score = [sum(np.floor(df["score"].head(N))) / len(df.head(N))
                      for df in df_list]
        fig_index.yaxis.bounds = (0.0, 10.0)
        fig_date.yaxis.bounds = (0.0, 10.0)
    else:  # Bedrift
        mean_score = [sum(np.floor(df["bedrift"].head(N))) / len(df.head(N))
                      for df in df_list]
        fig_index.yaxis.bounds = (0.0, 10.0)
        fig_date.yaxis.bounds = (0.0, 10.0)

    tot_score = [N*x for x in mean_score]

    mean_score_smooth = gaussian_filter1d(mean_score, sigma=smoothing.value)

    # Smoothing of datetime data
    def tofloat(d):
        return (d - date[0]).total_seconds()
    num_days = (datetime.today() - date[0]).days
    new_date = [date[0] + timedelta(days=d) for d in range(0, num_days)]
    f = np.interp(list(map(tofloat, new_date)),
                  list(map(tofloat, date)),
                  mean_score)
    mean_score_smooth_date = gaussian_filter1d(f, sigma=smoothing.value*30/5)

    source_index.data = dict(index=index,
                             date=date,
                             mean_score=mean_score,
                             tot_score=tot_score,
                             )
    source_smooth.data = dict(index=index,
                              date=date,
                              mean_score=mean_score_smooth,
                              tot_score=[N*x for x in mean_score_smooth],
                              )
    source_date.data = dict(date=new_date,
                            mean_score=mean_score_smooth_date,
                            tot_score=[N*x for x in mean_score_smooth_date],
                            )


# Widgets
name = Select(title="Name", value="Tor",
              options=["Tor", "Lars", "Jørn"])
decimal = Select(title="Score", value="Decimal",
                 options=["Decimal", "Integer", "Bedrift"])
N_slider = Slider(title="#Shots", value=60,
                  start=5, end=60, step=5)
smoothing = Slider(title="smoothing", value=5,
                   start=1, end=10, step=1)

# Initialize data
shot_list = {"Tor":  utils.generate_list("Tor"),
             "Lars": utils.generate_list("Lars"),
             "Jørn": utils.generate_list("Jørn")}
source_index = ColumnDataSource(data=dict())
source_date = ColumnDataSource(data=dict())
source_smooth = ColumnDataSource(data=dict())

# Initialize plot
fig_index = figure(x_range=[-len(shot_list[name.value]), 0],
                   x_axis_label='Index',
                   **FIGURE_CONFIG)
fig_index.xgrid.visible = False
utils.set_style(fig_index)

fig_date = figure(x_axis_type='datetime',
                  x_axis_label='Datetime',
                  **FIGURE_CONFIG)
utils.set_style(fig_date)

fig_bed = figure(x_axis_label='Score',
                 **FIGURE_CONFIG)
utils.set_style(fig_bed)

fig_hist = figure(x_axis_label='Score',
                  **HISTOG_CONFIG)

# Add plots
generate_data(0, 0, 0)

fig_index.scatter("index", "mean_score", source=source_index,
                  size=10, line_color='black')
fig_index.line("index", "mean_score", source=source_smooth)

fig_date.scatter("date", "mean_score", source=source_index,
                 size=10, line_color='black')
fig_date.line("date", "mean_score", source=source_date)

hist, edges = np.histogram(source_index.data["mean_score"][:])
hist_df = pd.DataFrame({"column": hist,
                        "left": edges[:-1],
                        "right": edges[1:],})
hist_src = ColumnDataSource(data=hist_df)
fig_hist.quad(top="column", bottom=0, left="left", right="right", source=hist_src)
#
# comp_tor = pd.read_csv("SHOTLOG/tor_comp.txt", sep=';')
# comp_tor["datetime"] = pd.to_datetime(comp_tor["datetime"])
# fig_date.scatter(comp_tor["datetime"], [score/60 for score in comp_tor["score"]],
#                  color='red', size=10, line_color='black')

all_data = shot_list["Tor"] #+ shot_list["Lars"] + shot_list["Jørn"]
bed_scores = [sum(np.floor(df["bedrift"])) / len(df["bedrift"])
              for df in all_data]
issf_scores = [sum(df["score"]) / len(df["score"])
               for df in all_data]

fig_bed.scatter(issf_scores, bed_scores)
fig_bed.xaxis.bounds = (0.0, 10.9)
fig_bed.yaxis.bounds = (0.0, 10.0)

# Update plot
name.on_change('value', generate_data)
decimal.on_change('value', generate_data)
N_slider.on_change('value', generate_data)
smoothing.on_change('value', generate_data)

# Add tooltip
hover = HoverTool(
    tooltips=[
        ('index', '@index'),
        ('mean', '@mean_score{(0.000)}'),
        ('score', '@tot_score{(0.0)}')],)
fig_index.add_tools(hover)

hover = HoverTool(
    tooltips=[
        ('date', '@date{%F}'),
        ('mean', '@mean_score{(0.000)}'),
        ('score', '@tot_score{(0.0)}')],
    formatters={'@date': 'datetime'},)
fig_date.add_tools(hover)

# Tabs
tab1 = Panel(child=fig_index, title="Index")
tab2 = Panel(child=fig_date, title="Datetime")
tab3 = Panel(child=fig_bed, title="ISSF vs Bedrift")
tab4 = Panel(child=fig_hist, title="Histogram")

tabs = Tabs(tabs=[tab1, tab2, tab3, tab4])

# Set up html layout
inputs = column(name, decimal, N_slider, smoothing)
curdoc().add_root(row(inputs, tabs, width=100))
curdoc().title = "10m Rifle"

