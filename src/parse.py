#!/bin/python
"""
This script provides a parser for MLLeder data and Skytteonline
into a Pandas DataFrame. If called from the terminal
it will parse the arugments and print out the parsed files.

Example:
   python parse.py SHOTLOG.TXT
   python parse.py SHOTLOG.skytteonline
"""
import pandas as pd
import re
from datetime import datetime
from numpy import floor, float32, zeros, sqrt

def parse(fname):
    if ".txt" in fname.lower():
        return parse_mlleder(fname)
    elif ".skytteonline" in fname.lower():
        return parse_skytteonline(fname)
    else:
        print("Error: Filename {} not supported.".format(fname))


def read_mlleder(fname):
    return parse_mlleder(fname)


def read_skytteonline(fname):
    return parse_skytteonline(fname)


def parse_mlleder(fname):
    """
    Read and filter a MLLeder-file into a Pandas DataFrame.
    MLLeder files are structured as Tab-delimited files in 'latin1' encoding,
    00  " 1:  Pr\xf8ve      " or "14:  30 Skudd   " (name: all)
    01  " 1:  Pr\xf8ve      " or "14:  30 Skudd   " (name: valid)
    02  "9.3 "                                      (score)
    03  " 22: 22: 23: 23"                           (pressure values)
    04  "20"                                        (?)
    05  "3.83"                                      (x mm)
    06  "-4.81"                                     (y mm)
    07  "6.1485"                                    (r mm)
    08  "\x0e 8"                                    (paper pulled mm)
    09  "11:00"                                     (HW)
    10  "    03:24.00"                              (time after start)
    11  "2018.09.11 - 18:34:40.36"                  (datetime)
    12  ""                                          (status)
    """
    df = pd.read_csv(fname,
                     sep='\t',
                     encoding='latin1',
                     header=None,
                     names=["name", "score", "x", "y", "r", "datetime"],
                     usecols=[1, 2, 5, 6, 7, 11])

    # Drop all extra shots
    df = df.dropna()
    df = df[df["name"].str.contains("Skudd")]
    df = df.reset_index(drop=True)

    # Drop shots after 60/40/30
    N_shot = int(df.name[0][5:7])
    if (len(df) >= N_shot):
        df = df.head(N_shot)
    elif (len(df) >= 60):
        if (len(df) != 60):
            print("Warning: {} Not enough shots. Load 60 shots. N={}".format(fname, len(df)))
        df = df.head(60)
    elif (len(df) >= 40):
        if (len(df) != 40):
            print("Warning: {} Not enough shots. Load 40 shots. N={}".format(fname, len(df)))
        df = df.head(40)
    elif (len(df) >= 30):
        if (len(df) != 30):
            print("Warning: {} Not enough shots. Load 30 shots. N={}".format(fname, len(df)))
        df = df.head(30)
    else:
        print("Error: {} Not enough shots. {}".format(fname, len(df)))
        quit()

    df["x"] = df["x"].astype(float32)
    df["y"] = df["y"].astype(float32)
    df["r"] = df["r"].astype(float32)
    df["score"] = calculate_score(df.r)
    df["bedrift"] = calculate_bedrift(df.r)
    df["datetime"] = pd.to_datetime(df["datetime"]).round("1s")
    df = df.drop(columns="name")

    return df


def parse_skytteonline(fname):
    """
    Read and filter a SkytteOnline-file into a Pandas DataFrame.
     0        1         2     3        4       5         6            7
     N    integer   decimal   r        x       y        time      time/shot
     2       10      10,1    2,23    -1,55   -1,62   17:29:48.57     47
    """
    df = pd.read_csv(fname,
                     sep='\t',
                     header=None,
                     names=["name", "score", "r", "x", "y", "time"],
                     usecols=[0, 2, 3, 4, 5, 6])

    N_shots = int(df["name"].iloc[-1])
    df = df.tail(N_shots)
    df = df.reset_index(drop=True)
    df = df.drop(columns="name")

    comma2dot = lambda x: x.replace(',', '.')
    df["r"] = df["r"].apply(comma2dot).astype(float32)
    df["x"] = df["x"].apply(comma2dot).astype(float32)
    df["y"] = df["y"].apply(comma2dot).astype(float32)
    df["score"] = df["score"].apply(comma2dot).astype(float32)
    #df["score"] = calculate_score( sqrt(df.x**2 + df.y**2) ) # too few digits of x/y/r in .skytteonline files
    #df["score"] = calculate_score( df.r )                    # too few digits of x/y/r in .skytteonline files
    df["bedrift"] = calculate_bedrift( sqrt(df.x**2 + df.y**2) )
    #df["bedrift"] = calculate_bedrift( df.r ) # alternate

    datetimes = []
    for time in df["time"]:
        datetime_str = fname2date(fname) + " " + time
        shot_datetime = datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S.%f')
        datetimes.append(shot_datetime)

    df["datetime"] = datetimes
    df["datetime"] = df["datetime"].round("1s")
    df = df.drop(columns="time")

    return df


def calculate_score(r):
    return round(10.9 - 0.1 * floor(r / 0.25), 1)


def calculate_bedrift(r):
    score = zeros(len(r))
    for i in range(len(r)):
        if (r[i] < 3.25):
            score[i] = round(10.9 - 0.1 * floor(r[i] / 0.325), 1)
        else:
            score[i] = round(9.9 - 0.1 * floor((r[i] - 3.25) / 0.45), 1)
    return score


def fname2date(fname):
    regexp = r'.*\/(\d*-\d*-\d*)-[a-z,A-Z,æøåÆØÅ]*-\d\.skytteonline'
    date_str = re.match(regexp, fname, re.UNICODE).group(1)
    return date_str


if __name__ == "__main__":
    import sys
    sys.argv.pop(0)
    for fname in sys.argv:
        data = parse(fname)
        print(data)
