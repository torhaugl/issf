#!/bin/python
import pandas as pd
from utils import generate_list
import numpy as np

for name in ["Tor", "Lars", "Jørn"]:
    dfs = generate_list(name)

    N = [int(len(df.score.head(60))) for df in dfs]
    sigma = [np.sqrt(np.sum((109 - df["score"].head(60)*10)**2) /(2*len(df.score.head(60))))  for df in dfs]
    scores = [int(round(df["score"].head(60).sum()*10)) for df in dfs]
    rs = [np.sqrt(df["x"].head(60).mean()**2 + df["y"].head(60).mean()**2) for df in dfs]
    sigma_rs = [np.sqrt(np.sum(df["r"].head(60)**2) / (2*len(df.r.head(60)))) for df in dfs]
    sigma_mean_rs = [np.sqrt(np.sum((df["r"].head(60)-df["r"].head(60).mean())**2) / (2*len(df.r.head(60)))) for df in dfs]
    optimals = [int(round(df["optimal"].head(60).sum()*10)) for df in dfs]
    sig_m_ss = [np.sqrt(np.sum((109 - df["optimal"].head(60)*10)**2) /(2*len(df.score.head(60))))  for df in dfs]
    print("result/{}_trening.txt".format(name))
    f = open("result/{}_trening.txt".format(name), "w")
    for (n, s, sig, r, sig_r, sig_m_r, optimal, sig_m_s) in zip(N, scores, sigma, rs, sigma_rs, sigma_mean_rs, optimals, sig_m_ss):
        f.write(" {} {} {} {} {} {} {} {}\n".format(n, s, round(sig, 2), round(r, 2), round(sig_r, 2), round(sig_m_r, 2), optimal, round(sig_m_s, 2)))
    f.close()