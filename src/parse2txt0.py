#!/bin/python
import pandas as pd
from utils import generate_list
import numpy as np
import datetime

for name in ["tor", "lars", "jørn"]:
    dfs = generate_list(name)

    datetimes = [df["datetime"][1] for df in dfs]
    N = [int(len(df.score.head(60))) for df in dfs]
    scores = [df["score"].head(60).sum() for df in dfs]

    print(f"result/{name}_trening.txt")
    f = open(f"result/{name}_trening.txt", "w")
    for (dt, n, s) in zip(datetimes, N, scores):
        f.write("{} {} {:.1f}\n".format(dt.date(), n, s))
    f.close()
