import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d

plt.subplots(1, 3)
plt.subplot(1,3,1)
plt.ylabel("Score")
i = 0
for name in ["Tor", "Lars", "Jørn"]:
    i += 1
    plt.subplot(1,3,i)
    plt.ylim(85*6, 105*6)
    data = np.loadtxt("result/{}_trening.txt".format(name))
    n = data[:,0]
    s = data[:,1] * 6
    s_optimal = data[:,6] * 6
    plt.scatter(range(-len(s), 0), s/n, s=10)
    y = gaussian_filter1d(s/n, 12.0)
    z = np.sqrt(gaussian_filter1d((s/n - y)**2, 12.0))
    plt.plot(range(-len(s), 0), y, 'r')
    plt.plot(range(-len(s), 0), y+2*z, 'r--')
    plt.plot(range(-len(s), 0), y-2*z, 'r--')

    ##Plot optimal scrore (r.mean = 0)
    ##plt.scatter(range(-len(s), 0), s_optimal/n, s=10, color='green')
    #plt.plot(range(-len(s), 0), gaussian_filter1d(s_optimal/n, 12.0), color='orange')
    ## Plot from Rayleigh sigma underestimates score slightly. Same trend
    #y = gaussian_filter1d((10.9-data[:,7]/10 * np.sqrt(np.pi/2.0))*60, 12.0)
    #z = gaussian_filter1d(data[:,7] * np.sqrt(2 - np.pi/2), 12.0)
    #plt.plot(range(-len(s), 0), y, 'g')
    #plt.plot(range(-len(s), 0), y+2*z, 'g--')
    #plt.plot(range(-len(s), 0), y-2*z, 'g--')

plt.subplot(1,3,2)
plt.yticks([])
plt.subplot(1,3,3)
plt.yticks([])
plt.savefig("figs/trening.png", dpi=300)