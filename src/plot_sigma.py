import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d

plt.subplots(1, 3)
plt.subplot(1,3,1)
plt.ylabel("Score")
i = 0
for name in ["Tor", "Lars", "Jørn"]:
    i += 1
    plt.subplot(1,3,i)
    plt.ylim(0, 4)
    data = np.loadtxt("result/{}_trening.txt".format(name))
    n = data[:,0]
    sigma = data[:,4]
    sigma_m = data[:,5]
    plt.scatter(range(-len(sigma), 0), sigma, s=10)
    plt.plot(range(-len(sigma), 0), gaussian_filter1d(sigma, 12.0), 'r')
    plt.scatter(range(-len(sigma_m), 0), sigma_m, s=10)
    plt.plot(range(-len(sigma_m), 0), gaussian_filter1d(sigma_m, 12.0), 'r')

plt.subplot(1,3,2)
plt.yticks([])
plt.subplot(1,3,3)
plt.yticks([])
plt.savefig("figs/trening_sigma.png", dpi=300)