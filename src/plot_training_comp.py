import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime

for name in ["jørn", "tor", "lars"]:
    training = pd.read_table(f"result/{name}_trening.txt", sep="\s+", names=["date", "N", "score"])
    training["date"] = [datetime.strptime(dt_str, '%Y-%m-%d') for dt_str in training.date]
    training.sort_values("date")
    print(training["date"])

    plt.scatter(training["date"], training["score"]/training["N"])
    plt.hlines([10.5], training["date"][0], training["date"].iloc[-1])
    plt.savefig("figs/{}_date.png".format(name))
    plt.clf()

    plt.scatter(range(len(training["date"])), training["score"]/training["N"], c=training["N"])
    plt.hlines([10.5], 0, len(training["date"]))
    plt.savefig("figs/{}_N.png".format(name))
    plt.clf()
