import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from parse import calculate_score
import utils

shot_list = {"Tor":  utils.generate_list("Tor"),
             "Lars": utils.generate_list("Lars"),
             "Jørn": utils.generate_list("Jørn")}

plt.subplots(len(shot_list.keys()), 1)
j = 0
for name in shot_list.keys():
    j += 1
    plt.subplot(1,len(shot_list.keys()), j)
    dfs = shot_list[name]
    if name == "Tor":
        dfs = dfs[-100:]
    elif name == "Lars":
        dfs = dfs[-300:]

    dfs = dfs[-100:]

    #nothing
    scores = []
    for df in dfs:
        x = df.x
        y = df.y
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores0 = scores
    #plt.hist(scores, bins=np.linspace(600, 625,4*25), label=name + str(" nothing"), density=True, alpha=0.5)

    #mean
    scores = []
    for df in dfs:
        x = df.x.copy() - df.x.mean()
        y = df.y.copy() - df.y.mean()
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" mean ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    #median
    scores = []
    for df in dfs:
        x = df.x.copy() - df.x.median()
        y = df.y.copy() - df.y.median()
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    print(np.max(scores))
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" median ") + str(round(np.mean(scores),1)), density=True, alpha=0.5)

    # every shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            if abs(x[i]) > abs(y[i]):
                x[i+1:] -= 0.25 * np.sign(x[i])
            else:
                y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" every1/1 ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # every 5 shot / 2
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        x10s = np.zeros(5)
        y10s = np.zeros(5)
        for i in range(len(df.x)-1):
            x10s[np.mod(i, 5)] = x[i]
            y10s[np.mod(i, 5)] = y[i]
            if (np.mod(i+1, 5) == 0):
                nx = round(np.mean(x10s) / 0.25, 0)
                ny = round(np.mean(y10s) / 0.25, 0)
                nx = np.clip(nx, -2, 2)
                ny = np.clip(ny, -2, 2)
                x[i+1:] -= 0.25 * nx
                y[i+1:] -= 0.25 * ny
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" every5/2 ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)
    # every 5 shot / 5
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        x10s = np.zeros(5)
        y10s = np.zeros(5)
        for i in range(len(df.x)-1):
            x10s[np.mod(i, 5)] = x[i]
            y10s[np.mod(i, 5)] = y[i]
            if (np.mod(i+1, 5) == 0):
                nx = round(np.mean(x10s) / 0.25, 0)
                ny = round(np.mean(y10s) / 0.25, 0)
                nx = np.clip(nx, -5, 5)
                ny = np.clip(ny, -5, 5)
                x[i+1:] -= 0.25 * nx
                y[i+1:] -= 0.25 * ny
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" every5/5 ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # every 10 shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        x10s = np.zeros(10)
        y10s = np.zeros(10)
        for i in range(len(df.x)-1):
            x10s[np.mod(i, 10)] = x[i]
            y10s[np.mod(i, 10)] = y[i]
            if (np.mod(i+1, 10) == 0):
                nx = round(np.mean(x10s) / 0.25, 0)
                ny = round(np.mean(y10s) / 0.25, 0)
                nx = np.clip(nx, -2, 2)
                ny = np.clip(ny, -2, 2)
                x[i+1:] -= 0.25 * nx
                y[i+1:] -= 0.25 * ny
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" every10/2 ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # every 10 shot / 5
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        x10s = np.zeros(10)
        y10s = np.zeros(10)
        for i in range(len(df.x)-1):
            x10s[np.mod(i, 10)] = x[i]
            y10s[np.mod(i, 10)] = y[i]
            if (np.mod(i+1, 10) == 0):
                nx = round(np.mean(x10s) / 0.25, 0)
                ny = round(np.mean(y10s) / 0.25, 0)
                nx = np.clip(nx, -5, 5)
                ny = np.clip(ny, -5, 5)
                x[i+1:] -= 0.25 * nx
                y[i+1:] -= 0.25 * ny
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" every10/5 ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # good shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score >= 9.7:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" good(>=9.7) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # good shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score >= 10.0:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" good(>=10.0) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # good shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score >= 10.2:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" good(>=10.2) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # bad shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score < 9.7:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" bad(<9.7) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)


    # bad shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score < 10.0:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" bad(<10.0) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    # bad shot
    scores = []
    for df in dfs:
        x = df.x.copy()
        y = df.y.copy()
        for i in range(len(df.x)-1):
            this_score = calculate_score(np.sqrt(x[i]**2 + y[i]**2))
            if this_score < 10.2:
                if abs(x[i]) > abs(y[i]):
                    x[i+1:] -= 0.25 * np.sign(x[i])
                else:
                    y[i+1:] -= 0.25 * np.sign(y[i])
        df.score = calculate_score(np.sqrt(x**2 + y**2))
        n = len(df.score.head(60))
        score = round(df.score.head(60).mean()*60, 1)
        scores.append(score)
    scores = np.array(scores)
    scores -= scores0
    plt.hist(scores, bins=range(-10, 11), label=str(" bad(<10.2) ") + str(round(np.mean(scores), 1)), density=True, alpha=0.5)

    plt.legend()

plt.savefig("figs/strategy.png")
