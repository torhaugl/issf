import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt
from scipy.stats import rayleigh, chi2
from scipy.special import i0


def calculate_score(r):
    return np.round(10.9 - 0.1 * np.floor(r / 0.25), 1)


def simulate_normal_score(mu, sigma1, sigma2, N_target=100000, N_shot=60):
    N = N_shot * N_target
    rng = default_rng()
    x = rng.normal(mu, sigma1, N)
    y = rng.normal(0, sigma2, N)
    r = np.sqrt(x**2 + y**2)
    points = 109 - 10*calculate_score(r)
    scores = [10.9*N_shot - np.sum(points[i:i+N_shot])/10 for i in range(0,len(points),N_shot)]
    return points, scores


def shooting_pdf(x, mu, sigma):
    # mu = 0: rayleigh.pdf(x/(4*sigma)) / (4*sigma)
    # assumes spherically symmetric
    lamb = mu/sigma
    z = x/(4*sigma)
    return i0(lamb*z) * z * np.exp(-(z**2 + lamb**2)/2) /(4*sigma)


mu, sigma1, sigma2 = 0.0, 1.4, 1.4
points, scores = simulate_normal_score(mu, sigma1, sigma2)
plt.subplots(2,1)
plt.subplot(2,1,1)
plt.hist(scores, bins=np.linspace(600,635,351), alpha=0.5, density=True)
plt.ylabel("PDF")
plt.xlabel("Total score (60)")
plt.subplot(2,1,2)
plt.hist(points, bins=np.linspace(0,30,30+1), alpha=0.5, density=True)
xs = np.linspace(0, 30, 1001)
plt.plot(xs, [shooting_pdf(x, mu, np.sqrt(sigma1*sigma2)) for x in xs])

mu, sigma1, sigma2 = 0.4, 1.4, 1.4
points, scores = simulate_normal_score(mu, sigma1, sigma2)
plt.subplot(2,1,1)
print(scores)
plt.hist(scores, bins=np.linspace(600,635,351), alpha=0.5, density=True)
plt.subplot(2,1,2)
plt.hist(points, bins=np.linspace(0,30,30+1), alpha=0.5, density=True)
plt.plot(xs, [shooting_pdf(x, mu, np.sqrt(sigma1*sigma2)) for x in xs])
plt.ylabel("PDF")
plt.xlabel("Score loss (x10)")

plt.tight_layout()
plt.savefig("figs/simulated_distribution.png")