#!/bin/python
import pandas as pd
from parse import read_mlleder, read_skytteonline
import glob

ISSF_TO_BEDRIFT = 1
BEDRIFT_TO_ISSF = ISSF_TO_BEDRIFT**(-1)


def generate_comp_list():
    comp_list = pd.read_csv("SHOTLOG/tor_comp.txt", sep=';', parse_dates=[0])
    comp_list["score"] = comp_list["score"] / comp_list["N"]
    comp_list["bedrift"] = comp_list["score"] * ISSF_TO_BEDRIFT


def generate_list(name):
    # Generate list of all shootings
    df_list = []

    for fname in glob.glob("SHOTLOG/{}/*.TXT".format(name)):
        df = read_mlleder(fname)
        df_list.append(df)

    for fname in glob.glob("SHOTLOG/{}/*.skytteonline".format(name)):
        df = read_skytteonline(fname)
        df_list.append(df)

    df_list = sorted(df_list, key=lambda x: x["datetime"][0])
    return df_list
