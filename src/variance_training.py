import numpy as np
import matplotlib.pyplot as plt

shots = np.loadtxt("result/Jørn_trening.txt", usecols=[1])
score = np.loadtxt("result/Jørn_trening.txt", usecols=[2])
score = score / shots * 60 - 600

# Rerun std/mean
maxN = 40

ns = range(3, maxN)
std = np.zeros(len(ns))
mean = np.zeros(len(ns))
s = [score[-n-1 - 0] for n in range(maxN)]
bedrift = [shots[-n-1] for n in range(maxN)]
for (i, n) in enumerate(ns):
    mean[i] = np.mean(s[1:n])
    std[i] = np.std(s[1:n])

# Remove outliers
keep = np.ones(len(s), dtype=bool)
new_s = []
for i in range(len(s)):
    if s[i] < mean[6] - 2*std[6]:
        keep[i] = False
    else:
        new_s.append(s[i])

plt.plot(ns, mean)
plt.plot(ns, mean - 2*std)
plt.plot(ns, mean + 2*std)
plt.scatter(range(1,len(s)+1), s, c = (mean[6] - 2 * std[6] < s))
#plt.scatter(range(1,21), s, c = bedrift)
plt.savefig("figs/std.png")

print("Remove outliers: ", sum(keep == False))
s = np.array(new_s)

# Rerun std/mean
ns = range(3, len(s))
std = np.zeros(len(ns))
mean = np.zeros(len(ns))
for (i, n) in enumerate(ns):
    mean[i] = np.mean(s[1:n])
    std[i] = np.std(s[1:n])


#plt.plot(ns, std)
plt.plot(ns, mean)
plt.plot(ns, mean - 2*std)
plt.plot(ns, mean + 2*std)
plt.scatter(range(1,len(s)+1), s)#, c = (mean[6] - 2 * std[6] < s))
#plt.scatter(range(1,21), s, c = bedrift)
#plt.savefig("figs/std.png")
